const { ServiceBroker } = require("moleculer");
const DbService = require("moleculer-db");
const SqlAdapter = require("moleculer-db-adapter-sequelize");
const Sequelize = require("sequelize");
const message = require('../../lib/message');
const process = require('../../mixins/db.config');
const bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');
const { uuid } = require('uuidv4');
const nodemailer = require("nodemailer");
const role = message.roles.merchant;


module.exports = {
    name: 'merchants',
    mixins: [DbService],

    adapter : new SqlAdapter(process.mysql.database, process.mysql.user, process.mysql.password, {
        host: process.mysql.host,
        dialect: 'mysql' /* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */,
        
    }),
    
    model: {
        name: "merchant",
        define: {
            merchantName:Sequelize.STRING,
            merchantLogo: Sequelize.STRING,
            password: Sequelize.STRING,
			merchantWebsite: Sequelize.STRING,
			contactPersonForSparks: Sequelize.STRING,
			contactEmail: Sequelize.STRING,
			mobileForSparks: Sequelize.STRING,
			notes: Sequelize.STRING,
			status: Sequelize.STRING,
            bank	: Sequelize.JSON,
            connect_id: Sequelize.STRING,
            stripeMerchantId: Sequelize.STRING,
            resetPasswordCode:Sequelize.STRING,
            createdBy: Sequelize.INTEGER,
            updatedBy: Sequelize.INTEGER,
            createdAt : Sequelize.DATE,
            updatedAt : Sequelize.DATE
        },
        options: {}
    },

    actions: {

        signup: {
            rest: {
				method: "POST",
				path: "/signup"
            },
            async handler(ctx) {
                try{
                    const Auth = ctx.meta.user;
                    if(Auth == 'forbidden'){
                        return message.message.UNAUTHORIZED;
                    }
                    if(Auth.role == 1){
                      
                            const password = '123456';
                            const merchantName = ctx.params.merchantName;
                            const merchantLogo = ctx.params.merchantLogo;
                            const merchantSignUpEmail = ctx.params.merchantSignUpEmail;
                            const merchantWebsite = ctx.params.merchantWebsite;
                            const contactPersonForSparks = ctx.params.contactPersonForSparks || null;
                            const contactEmail = ctx.params.contactEmail || null;
                            const mobileForSparks = JSON.stringify(ctx.params.mobileForSparks) || null;
                            const notes = ctx.params.notes || null;
                            const status = 1;
                            const bank = ctx.params.bank || null;
                            const connect_id = ctx.params.connect_id || null;
                            const stripeMerchantId = ctx.params.stripeMerchantId || null;
                            const resetPasswordCode = null;
                            const createdBy = 1;
                            const updatedBy = 1;
                            const hash = await bcrypt.hash(password,10);
                            const checkMerchant = `select * from merchants where merchantName = '${merchantName}' and merchantSignUpEmail = '${merchantSignUpEmail}'`;
                            const [checkMerchantress] = await this.adapter.db.query(checkMerchant)
                            if(checkMerchantress != ''){
                                return process.message.UNIQMERCHANT;
                            }else{
                                const saveMerchant = `insert into merchants(merchantName,merchantLogo,merchantSignUpEmail,password,merchantWebsite,contactPersonForSparks,contactEmail,mobileForSparks,notes,status,bank,connect_id,stripeMerchantId,resetPasswordCode,createdBy,updatedBy) values('${merchantName}','${merchantLogo}','${merchantSignUpEmail}','${hash}','${merchantWebsite}','${contactPersonForSparks}','${contactEmail}','${mobileForSparks}','${notes}','${status}','${bank}','${connect_id}','${stripeMerchantId}','${resetPasswordCode}','${createdBy}','${updatedBy}')`;
                                const [saveMerchantress] = await this.adapter.db.query(saveMerchant);
        
                                const data = {
                                    id : saveMerchantress,
                                    merchantName : ctx.params.merchantName,
                                    merchantLogo: ctx.params.merchantLogo,
                                    merchantSignUpEmail: ctx.params.merchantSignUpEmail,
                                    merchantWebsite: ctx.params.merchantWebsite,
                                    contactPersonForSparks: ctx.params.contactPersonForSparks,
                                    contactEmail: ctx.params.contactEmail,
                                    mobileForSparks: ctx.params.mobileForSparks,
                                    notes: ctx.params.notes,
                                    bank: ctx.params.bank,
                                    connect_id: ctx.params.connect_id,
                                    stripeMerchantId:ctx.params.stripeMerchantId,
                                }
                                const successMessage = {
                                    success:true,
                                    statusCode:200,
                                    data:data,
                                    message:'Success'
                                }
                                return successMessage
                            }
                  }else{
                        return message.message.PERMISSIONDENIDE;
                    }
                }catch(error){
                    const errMessage = {
                        success:false,
                        statusCode:409,
                        error:error.errors,

                    }
                    return errMessage;
                }
			}
        },

        signin: {
            rest: {
				method: "POST",
				path: "/signin"
            },
            async handler(ctx) {
                try{
                    const merchantSignUpEmail = ctx.params.merchantSignUpEmail;
                    const password = ctx.params.password;
                    const checkMerchant = `select * from merchants where merchantSignUpEmail = '${merchantSignUpEmail}'`;
                    const [checkMerchantress] = await this.adapter.db.query(checkMerchant);
                    if(checkMerchantress != ''){
                        const pwd = checkMerchantress[0].password;
                        var matchResult = await bcrypt.compare(password,pwd);
                        if(matchResult == true){
                            const merchantID = checkMerchantress[0].id;

                            var token = jwt.sign({
                                id: merchantID,
                                email:checkMerchantress[0].email,
                                status: checkMerchantress[0].status,
                                role:role,
                            }, 'secretkey', { expiresIn: '12h' });
                            const merchantdata = {
                                id: merchantID,
                                merchantName: checkMerchantress[0].merchantName,
                                merchantLogo: checkMerchantress[0].merchantLogo,
                                merchantSignUpEmail: checkMerchantress[0].merchantSignUpEmail,
                                merchantWebsite: checkMerchantress[0].merchantWebsite,
                                contactPersonForSparks: checkMerchantress[0].contactPersonForSparks,
                                contactEmail: checkMerchantress[0].contactEmail,
                                mobileForSparks: checkMerchantress[0].mobileForSparks,
                                notes: checkMerchantress[0].notes,
                                status: checkMerchantress[0].status,
                                connect_id: checkMerchantress[0].connect_id,
                                stripeMerchantId: checkMerchantress[0].stripeMerchantId,
                                createdBy: checkMerchantress[0].createdBy,
                                updatedBy: checkMerchantress[0].updatedBy,
                                createdAt:checkMerchantress[0].createdAt,
                                updatedAt:checkMerchantress[0].updatedAt,
                                token: token
                            }
           
                            const successMessage = {
                                  success:true,
                                  statusCode:200,
                                  data:merchantdata,
                                  message:'Success'
                            }
                            const checkToken  = `select * from authentications where user_id = '${merchantID}' and type = '${'merchant'}'`;
                            const [checkTokenress] = await this.adapter.db.query(checkToken);
                            if(checkTokenress != ''){
                                const updateToken = `update authentications set token = '${token}' where user_id = '${merchantID}'`
                                const [updateTokenress] = await this.adapter.db.query(updateToken);
                                if(updateTokenress.affectedRows >= 1){
                                    return successMessage
                                }else{
                                    return message.message.LOGINFAIL;
                                }
                            }else {
                                const saveToken = `insert into authentications(type,user_id,token) values('${'merchant'}','${merchantID}','${token}')`
                                const [saveTokenress] = await this.adapter.db.query(saveToken);
                                if(saveTokenress){
                                    return successMessage;
                                }else {
                                    return message.message.LOGINFAIL;
                                }
                            }
                        }else {
                            return message.message.PASSWORDDUP;
                        }
                    }else {
                        return message.message.USERNOTFOUND;
                    }
                }catch(error){
                    const errMessage = {
                        success:false,
                        statusCode:409,
                        error:error.errors,

                    }
                    return errMessage;
                }
			}
        },

        profile: {
            rest: {
				method: "GET",
				path: "/profile"
            },
            async handler(ctx,res,req) {
                try{
                    const Auth = ctx.meta.user;
                    if(Auth != 'forbidden'){
                        const merchantId = Auth.id;
                        const findmerchant = `select * from merchants where id = '${merchantId}'`;
                        const [findmerchantress] = await this.adapter.db.query(findmerchant);
                        // return findmerchantress
                        const merchantProfile = {
                            id: findmerchantress[0].id,
                            merchantName: findmerchantress[0].merchantName,
                            merchantLogo: findmerchantress[0].merchantLogo,
                            merchantSignUpEmail: findmerchantress[0].merchantSignUpEmail,
                            merchantWebsite: findmerchantress[0].merchantWebsite,
                            contactPersonForSparks: findmerchantress[0].contactPersonForSparks,
                            contactEmail: findmerchantress[0].contactEmail,
                            mobileForSparks: findmerchantress[0].mobileForSparks,
                            notes: findmerchantress[0].notes,
                            status: findmerchantress[0].status,
                            connect_id: findmerchantress[0].connect_id,
                            stripeMerchantId: findmerchantress[0].stripeMerchantId,
                            createdBy: findmerchantress[0].createdBy,
                            updatedBy: findmerchantress[0].updatedBy,
                            createdAt:findmerchantress[0].createdAt,
                            updatedAt:findmerchantress[0].updatedAt,
                        }
                            const successMessage = {
                                success:true,
                                statusCode:200,
                                data:merchantProfile
                            }
                        if(findmerchantress == ''){
                            return successMessage;
                        }else {
                            return successMessage;
                        }
                    }else{
                        return message.message.UNAUTHORIZED;
                    }
                }catch(error){
                    const errMessage = {
                        success:false,
                        statusCode:409,
                        error:error.errors,

                    }
                    return errMessage;
                }
			}
        },
        
        merchantList: {
            rest: {
				method: "GET",
				path: "/merchantList"
            },
            async handler(ctx) {
                try{
                    const merchantDATA = [];
                    const ACTIVITY = []
                    const Auth = ctx.meta.user;
                    if(Auth != 'forbidden'){
                        if(Auth.role == 1){
                            const merchatActivities = `select * from merchant_activities ORDER BY title ASC`
                            const [merchatActivitiesress] = await this.adapter.db.query(merchatActivities);
                            if(merchatActivitiesress == ''){
                                return "blank"
                            }else{
                                for(let mactiv of merchatActivitiesress){
                                    const merchantId = mactiv.merchantId;
                                    const activityId = mactiv.activityId;
                                    
                                    const activity = `select * from activities where id = '${activityId}' `;
                                    const [activityress] = await this.adapter.db.query(activity);
                                    if(activityress != ''){
                                        const Activcat = [];
                                        for(let atv of activityress){
                                            const aId = atv.id
                                            const categories = `select acr.*,ac.id,ac.name from activity_category_relations as acr inner join activity_categories as ac on acr.activityCategoryId = ac.id where acr.activityId = '${aId}'`
                                            // const categories = `select * from activity_category_relations where activityId = '${aId}'`
                                            const [categoriesress] = await this.adapter.db.query(categories);
                                            if(categoriesress == ''){
                                                const activitycategories = {
                                                    "activity_category_relations":categoriesress
                                                }
                                                Activcat.push(activitycategories) 
                                            }else{
                                                const activitycategories = {
                                                    "activity_category_relations":categoriesress
                                                }
                                                Activcat.push(activitycategories)   
                                            }

                                            const activityData = {
                                                id: atv.id,
                                                title:atv.title,
                                                activitycategories:Activcat
                                            }
                                            const merchantData = `select id,merchantName,merchantsignupemail from merchants where id = '${merchantId}'`
                                            const [merchantDataress] = await this.adapter.db.query(merchantData);
                                         
                                            const merlist = {
                                                "id": mactiv.id,
                                                "merchantId": mactiv.merchantId,
                                                "title": mactiv.title,
                                                "images": mactiv.images,
                                                "status": mactiv.status,
                                                "createdAt": mactiv.createdAt,
                                                "updatedAt": mactiv.updatedAt,
                                                "activity": activityData,
                                                "merchant": merchantDataress
                                            } 
                                            merchantDATA.push(merlist)
                                        }
                                            // ACTIVITY.push(activityData)

                                    }else{
                                        return 'blank activity'
                                    }
                                }
                                return merchantDATA
                            }
                        }else{
                            return message.message.PERMISSIONDENIDE
                        }
                    }else{
                        return message.message.UNAUTHORIZED;
                    }
                }catch(error){
                    const errMessage = {
                        success:false,
                        statusCode:409,
                        error:error.errors,

                    }
                    return errMessage;
                }
			}
        },
        //================= list by id ====================
        list: {
            rest: {
				method: "GET",
				path: "/list/:id"
            },
            async handler(ctx) {
                try{
                    const Auth = ctx.meta.user;
                    const merchantId = ctx.params.id;
                    if(Auth != 'forbidden'){
                        if(Auth.role == 1){
                            const findmerchant = `select * from merchants where id = '${merchantId}'`;
                            const [findmerchantress] = await this.adapter.db.query(findmerchant);
                                const List = {
                                    id: findmerchantress[0].id,
                                    merchantName: findmerchantress[0].merchantName,
                                    merchantLogo: findmerchantress[0].merchantLogo,
                                    merchantSignUpEmail: findmerchantress[0].merchantSignUpEmail,
                                    merchantWebsite: findmerchantress[0].merchantWebsite,
                                    contactPersonForSparks: findmerchantress[0].contactPersonForSparks,
                                    contactEmail: findmerchantress[0].contactEmail,
                                    mobileForSparks: findmerchantress[0].mobileForSparks,
                                    notes: findmerchantress[0].notes,
                                    status: findmerchantress[0].status,
                                    connect_id: findmerchantress[0].connect_id,
                                    stripeMerchantId: findmerchantress[0].stripeMerchantId,
                                    createdBy: findmerchantress[0].createdBy,
                                    updatedBy: findmerchantress[0].updatedBy,
                                    createdAt:findmerchantress[0].createdAt,
                                    updatedAt:findmerchantress[0].updatedAt,
                                }
                                const successMessage = {
                                    success:true,
                                    statusCode:200,
                                    data:List,
                                    message:'Success'
                                }
                            if(findmerchantress == ''){
                                return successMessage;
                            }else {
                                return successMessage;
                            }
                        }else{
                            return message.message.PERMISSIONDENIDE
                        }
                    }else{
                        return message.message.UNAUTHORIZED;
                    }
                }catch(error){
                    const errMessage = {
                        success:false,
                        statusCode:409,
                        error:error.errors,

                    }
                    return errMessage;
                }
			}
        },

        reset_password: {
            rest: {
				method: "POST",
				path: "/reset_password"
            },
            async handler(ctx,res,req) {
                try{
                    const email = ctx.params.merchantSignUpEmail;
                    const password = ctx.params.password;
                    const findEmail = `select * from merchants where merchantSignUpEmail = '${email}'`;
                    const [findEmailress] = await this.adapter.db.query(findEmail);
                    if(findEmailress != ''){
                        // return findEmailress
                        const hash = await bcrypt.hash(password,10);
                        const setPassword = `update merchants set password = '${hash}' where merchantSignUpEmail = '${email}'`;
                        const [setPassress] = await this.adapter.db.query(setPassword);
                        if(setPassress.affectedRows >= 1){
                            return message.message.RESETPASSWORD;
                        }else{
                            return message.message.RESETPASSWORDNOT;
                        }
                    }else{
                        return message.message.EMAILNOTFOUND;
                    }
                }catch(error){
                    return error
                }
			}
        },

        // forget_password: {
        //     rest: {
		// 		method: "POST",
		// 		path: "/forget_password"
        //     },
              
        //     async handler(ctx,res,req) {
        //         try{
        //             const email = ctx.params.merchantSignUpEmail;
        //             const findEmail = `select * from merchants where merchantSignUpEmail = '${email}'`;
        //             const [findEmailress] = await this.adapter.db.query(findEmail);
        //             if(findEmailress == ''){
        //                 return message.message.EMAILNOTFOUND;
        //             }else {
        //                 const token = uuid();
        //                 const merchantName = findEmailress[0].merchantName;
        //                 var today = new Date();
        //                 var dd = String(today.getDate()).padStart(2, '0');
        //                 var mm = String(today.getMonth() + 1).padStart(2, '0');
        //                 var yyyy = today.getFullYear();
        //                 today = yyyy + '-' + mm + '-' + dd;
                    
        //                 const updateUserToken = `UPDATE merchants SET resetPasswordCode = '${token}', resetPasswordExpire = '${today}' WHERE merchantSignUpEmail = '${email}'`;
                        
        //                 const [Emailresult] = await this.adapter.db.query(updateUserToken);
        //                 if(Emailresult){
        //                     const successMessage = {
        //                         success:true,
        //                         statusCode:200,
        //                         message   : "Email Sent. Go to your email account and reset password"
        //                     }
        //                     return  successMessage;
        //                 }else{
        //                     const successMessage = {
        //                         success:false,
        //                         statusCode:200,
        //                         message   : "Something Went Wrong"
        //                     }
        //                     return  successMessage;
        //                 }
        //             }
        //         }catch(error){
        //             return error
        //         }
		// 	}
        // },

        
        
        
    }
    
}
