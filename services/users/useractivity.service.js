const { ServiceBroker } = require("moleculer");
const DbService = require("moleculer-db");
const SqlAdapter = require("moleculer-db-adapter-sequelize");
const Sequelize = require("sequelize");
const process = require('../../mixins/db.config');
const message = require('../../lib/message');
const bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');
const { uuid } = require('uuidv4');
const nodemailer = require("nodemailer");
const role = message.roles.user;
var moment = require('moment');


module.exports = {
    name: 'useractivity',
    mixins: [DbService],

    adapter : new SqlAdapter(process.mysql.database, process.mysql.user, process.mysql.password, {
        host: process.mysql.host,
        dialect: 'mysql' /* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */,
        
    }),
    
    model: {
        name: "activitie",
        define: {
            // id:Sequelize.INTEGER,
            title: Sequelize.STRING,
            description: Sequelize.STRING,
            images: Sequelize.STRING,
			activityType: Sequelize.STRING,
			letterCollected: Sequelize.STRING,
			isBestSeller: Sequelize.INTEGER,
			bestSellerDuration: Sequelize.STRING,
			bestSellerStartDate: Sequelize.STRING,
			bestSellerEndDate: Sequelize.STRING,
			suggestionHeader: Sequelize.STRING,
            status: Sequelize.INTEGER,
            createdAt : Sequelize.DATE,
            updatedAt : Sequelize.DATE
        },
        options: {}
    },

    actions: {

        activityCategoryList: {
            rest: {
				method: "GET",
				path: "/activityCategoryList"
            },
            async handler(ctx) {
                try{
                        const List = [];
                        const sql = `select * from activity_categories`
                        const [searcActivityress] = await this.adapter.db.query(sql);
                        // for(let key of searcActivityress){
                        //     // return key
                        //     const list = {
                        //         id:key.id,
                        //         name:key.name,
                        //         images:JSON.parse(key.images)
                        //     }
                        //     List.push(list);
                        // }
                        const successMessage = {
                            success:true,
                            status: 200,
                            data: searcActivityress,
                            message:'Success'
                        }
                        return successMessage
                        if(searcActivityress == ''){
                            return successMessage;
                        }else{
                            return successMessage;
                        }
                }catch(error){
                    return error
                }
			}
        }, 

        UserActivityList: {
            rest: {
				method: "POST",
				path: "/UserActivityList"
            },
            async handler(ctx,res,req) {
                try{
                    const userId = ctx.params.user_id;
                    const findUser= `SELECT * FROM users as U JOIN user_preferences as UP ON U.id = UP.userId WHERE U.id = '${userId}'`;
                    const [UserResult] = await this.adapter.db.query(findUser);
                    if(UserResult != ''){  
                        var ActivityCategory = UserResult[0].activityCategories;
                        if(ActivityCategory != null){
                            var strArray = ActivityCategory.split(",");
                            var activityArray = [];
                            for(var i = 0; i < strArray.length; i++){   
                                const activitieData = `SELECT * FROM activities WHERE id = '${strArray[i]}'`;
                                const [ActivityResult] = await this.adapter.db.query(activitieData);
                                if(ActivityResult != ''){
                                    const ACData = `SELECT * FROM activity_category_relations as acr JOIN activity_categories as ac ON acr.activityCategoryId = ac.id WHERE acr.activityId = '${ActivityResult[0].id}'`;    

                                    const [ACResult] = await this.adapter.db.query(ACData); 
                                    if(ACResult != ''){
                                        var acArray = [];
                                        for(let dat of ACResult){
                                            const data = {
                                                activity_category_id : dat.id,
                                                activity_category_name                 : dat.name,
                                                activity_category_images               : dat.images
                                            }
                                            acArray.push(data)
                                        }
                                        console.log(acArray)
                                    }else{
                                        var acArray = [];
                                    }
                                    const Result = {
                                        id              : ActivityResult[0].id,
                                        title           : ActivityResult[0].title,
                                        description     : ActivityResult[0].description,
                                        images          : ActivityResult[0].images,
                                        activityType    : ActivityResult[0].activityType,
                                        letterCollected : ActivityResult[0].letterCollected,
                                        isBestSeller    : ActivityResult[0].isBestSeller,
                                        bestSellerDuration : ActivityResult[0].bestSellerDuration,
                                        bestSellerStartDate: ActivityResult[0].bestSellerStartDate,
                                        bestSellerEndDate  : ActivityResult[0].bestSellerEndDate,
                                        suggestionHeader   : ActivityResult[0].suggestionHeader,
                                        status          : ActivityResult[0].status,
                                        createdAt       : ActivityResult[0].createdAt,
                                        updatedAt       : ActivityResult[0].updatedAt,
                                        activity_categories : acArray
                                    }
                                    activityArray.push(Result);
                                }else{
                                    const successMessage = {
                                        success : false,
                                        message : 'empty',
                                    }
                                    return successMessage;        
                                }
                            }

                            const response = {
                                success : true,
                                message : 'list data',
                                data    : activityArray
                            }
                            return response;
                        }else{
                            const successMessage = {
                                success : false,
                                message : 'Please update your activity first',
                            }
                            return successMessage;  
                        }
                    }else{
                        return message.message.USERNOTFOUND;
                    }
                }catch(error){
                    return error
                }
			}
        }, 
        
        merchantActivity: {
            rest: {
				method: "POST",
				path: "/merchantActivity"
            },
            async handler(ctx,res,req) {
                try{
                    const activityId = ctx.params.activityId;
                    const Sql = `SELECT * FROM merchant_activities WHERE activityId = '${activityId}'`;
                    const [SqlResult] = await this.adapter.db.query(Sql);
                    if(SqlResult != ''){ 
                            const response = {
                                success : true,
                                message : 'list data',
                                data    : SqlResult
                            }
                            return response;
                    }else{
                        const successMessage = {
                            success : false,
                            message : 'Empty',
                            data    : []    
                        }
                        return successMessage;  
                    }
                }catch(error){
                    return error
                }
			}
        }, 

        
    }
    
}