const { ServiceBroker } = require("moleculer");
const DbService = require("moleculer-db");
const SqlAdapter = require("moleculer-db-adapter-sequelize");
const Sequelize = require("sequelize");
const jwt = require('jsonwebtoken');
const process = require('../../mixins/db.config');
const message = require('../../lib/message');


module.exports = {
    name: 'merchant_activities',
    mixins: [DbService],

    adapter : new SqlAdapter(process.mysql.database, process.mysql.user, process.mysql.password, {
        host: process.mysql.host,
        dialect: 'mysql' /* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */,
        
    }),
    
    model: {
        name: "merchant_activitie",
        define: {
            // id:Sequelize.INTEGER,
            merchantId: Sequelize.INTEGER,
            activityId: Sequelize.INTEGER,
            outletIds: Sequelize.JSON,
			title: Sequelize.STRING,
			images: Sequelize.JSON,
			quantity: Sequelize.INTEGER,
			description: Sequelize.STRING,
			activityDuration: Sequelize.INTEGER,
			isReservationRequired: Sequelize.STRING,
			isAnniversarySpecial: Sequelize.STRING,
            averageRating: Sequelize.INTEGER,
            isBestSeller: Sequelize.STRING,
            isAnnisparksCommissionversarySpecial: Sequelize.STRING,
            sparksCommissionLastUpdated: Sequelize.STRING,
            sparksCommissionLastUpdateReason: Sequelize.STRING,
            merchantRanking: Sequelize.STRING,
            rankingStartDate: Sequelize.DATE,
            rankingEndDate: Sequelize.DATE,
            isFeatured: Sequelize.STRING,
            isFeaturedStartDate: Sequelize.DATE,
            isFeaturedEndDate: Sequelize.DATE,
            expectations: Sequelize.TEXT,
            additionalInfo: Sequelize.TEXT,
            terms: Sequelize.TEXT,
            howToRedeem: Sequelize.TEXT,
            status: Sequelize.INTEGER,
            createdBy: Sequelize.INTEGER,
            updatedBy: Sequelize.INTEGER,
            createdAt : Sequelize.DATE,
            updatedAt : Sequelize.DATE
        },
        options: {}
    },
    model: {
        name: "regular_deal",
        define: {
            merchantId: Sequelize.INTEGER,
            title: Sequelize.INTEGER,
            description: Sequelize.JSON,
			originalPrice: Sequelize.STRING,
			discountedPrice: Sequelize.JSON,
			discountedPercentage: Sequelize.INTEGER,
			discountAmount: Sequelize.STRING,
			validityPeriod: Sequelize.INTEGER,
			status: Sequelize.STRING,
			createdBy: Sequelize.STRING,
            modifiedBy: Sequelize.INTEGER,
            createdAt : Sequelize.DATE,
            updatedAt : Sequelize.DATE
        },
        options: {}
    },

    actions: {

        create: {
            rest: {
				method: "POST",
				path: "/create"
            },
            async handler(ctx) {
                try{
                    const Auth = ctx.meta.user;
                    const adminId = Auth.role;
                    if(Auth == 'forbidden'){
                        return message.message.UNAUTHORIZED;
                    }
                    if(adminId == 1){
                        const merchantId = ctx.params.merchantId;//
                        const activityId = ctx.params.activityId;//
                        const outletIds = JSON.stringify(ctx.params.outlets);//
                        const title = ctx.params.title;//
                        const images = JSON.stringify(ctx.params.images);//
                        const quantity = ctx.params.quantity || 1;
                        const description = ctx.params.description;//
                        const activityDuration = ctx.params.activityDuration;//
                        const isReservationRequired = ctx.params.isReservationRequired || 0;
                        const isAnniversarySpecial = ctx.params.isAnniversarySpecial;//
                        const averageRating = ctx.params.averageRating || 4;
                        const isBestSeller = ctx.params.isBestSeller || 0;
                        const sparksCommission = ctx.params.sparksCommission || 10;
                        const sparksCommissionLastUpdated = ctx.params.sparksCommissionLastUpdated || 1;
                        const sparksCommissionLastUpdateReason = ctx.params.sparksCommissionLastUpdateReason || 'sparksCommissionLastUpdateReason Text';
                        const merchantRanking = ctx.params.merchantRanking || 4;
                        const rankingStartDate = ctx.params.rankingStartDate;
                        const rankingEndDate = ctx.params.rankingEndDate;
                        const isFeatured = ctx.params.isFeatured || 0;
                        const isFeaturedStartDate = ctx.params.isFeaturedStartDate;
                        const isFeaturedEndDate = ctx.params.isFeaturedEndDate;
                        const expectations = ctx.params.expectations;//
                        const additionalInfo = ctx.params.additionalInfo;//
                        const terms = ctx.params.terms;//
                        const howToRedeem = ctx.params.howToRedeem;//
                        const status = 1;
                        const createdBy = adminId;
                        const updatedBy = adminId;
                        //=========== deals ==============
                        const dealsTitle = ctx.params.title;
                        const dealsdescription = ctx.params.description;
                        const dealsoriginalPrice = ctx.params.originalPrice;
                        const dealsdiscountedPrice = ctx.params.discountedPrice;
                        const dealsdiscountedPercentage = ctx.params.discountedPercentage;
                        const dealsdiscountAmount = ctx.params.discountAmount;
                        const dealsvalidityPeriod = ctx.params.validityPeriod;

                        const checkTitle = `select * from merchant_activities where title = '${title}'`
                        const [checkTitleress] = await this.adapter.db.query(checkTitle);
                        if(checkTitleress != ""){
                            return message.message.ALREADYTITLE;
                        }else{
                            const merchantActivity = `insert into merchant_activities(merchantId,activityId,outletIds,title,images,quantity,description,activityDuration,isReservationRequired,isAnniversarySpecial,averageRating,isBestSeller,sparksCommission,sparksCommissionLastUpdated,sparksCommissionLastUpdateReason,merchantRanking,isFeatured,expectations,additionalInfo,terms,howToRedeem,status,createdBy,updatedBy) values('${merchantId}','${activityId}','${outletIds}','${title}','${images}','${quantity}','${description}','${activityDuration}','${isReservationRequired}','${isAnniversarySpecial}','${averageRating}','${isBestSeller}','${sparksCommission}','${sparksCommissionLastUpdated}','${sparksCommissionLastUpdateReason}','${merchantRanking}','${isFeatured}','${expectations}','${additionalInfo}','${terms}','${howToRedeem}','${status}','${createdBy}','${updatedBy}')`;
                            
                            const [merchantActivityress] = await this.adapter.db.query(merchantActivity);
                            if(merchantActivityress){
                                // const regularDeal = `insert into regular_deals(merchantId,title,description,originalPrice,discountedPrice,discountedPercentage,discountAmount,validityPeriod,status,createdBy,modifiedBy) values('${merchantId}','${dealsTitle}','${dealsdescription}','$deals{originalPrice}','${dealsdiscountedPrice}','${dealsdiscountedPercentage}','${dealsdiscountAmount}','${dealsvalidityPeriod}','${status}','${createdBy}','${updatedBy}')`
                                // const [regularDealsress] = await this.adapter.db.query(regularDeal);
                                
                                  return message.message.SAVE;
                            }else{
                                const successMessage = {
                                    success:false,
                                    status: 500,
                                    message:'Not save'
                                }
                                return successMessage;
                            }

                        }
                    }else{
                        return message.message.PERMISSIONDENIDE;
                    }
                }catch(error){
                    const errMessage = {
                        success:false,
                        statusCode:409,
                        error:error.errors,
                    }
                    return errMessage;

                }
			}
        },

        listing: {
            rest: {
				method: "GET",
				path: "/listing/:id"
            },
            async handler(ctx,res,req) {
                try{
                    const Auth = ctx.meta.user;
                    const merchantId = ctx.params.merchantId;
                    if(Auth != 'forbidden'){
                        if(Auth.role == 1){
                            const findmerchant = `select * from merchant_activities where merchantId = '${merchantId}'`;
                            const [findmerchantress] = await this.adapter.db.query(findmerchant);
                            if(findmerchantress == ''){
                                return successMessage;
                            }else {
                                const fintmerchatn1 = `select * from merchants where id = '${merchantId}'`;
                                const [fintmerchatnress1] = await this.adapter.db.query(fintmerchatn1);
                                if(fintmerchatnress1 == ''){
                                    return 'not found'
                                }else{
                                    const Data = {
                                        "id": findmerchantress[0].id,
                                        "merchantRanking": findmerchantress[0].merchantRanking,
                                        "rankingStartDate": findmerchantress[0].rankingStartDate,
                                        "rankingEndDate": findmerchantress[0].rankingEndDate,
                                        "merchant": {
                                            "id":fintmerchatnress1[0].id ,
                                            "merchantName": fintmerchatnress1[0].merchantName
                                        }
                                    }
                                    return Data;
                                }
                            }
                        }else{
                            return message.message.PERMISSIONDENIDE;
                        }
                    }else{
                        return message.message.UNAUTHORIZED;
                    }
                }catch(error){
                    const errMessage = {
                        success:false,
                        statusCode:409,
                        error:error.errors,

                    }
                    return errMessage;
                }
			}
        },

    },
    
}
