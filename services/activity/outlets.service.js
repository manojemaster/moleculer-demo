const { ServiceBroker } = require("moleculer");
const DbService = require("moleculer-db");
const SqlAdapter = require("moleculer-db-adapter-sequelize");
const Sequelize = require("sequelize");
const jwt = require('jsonwebtoken');
const process = require('../../mixins/db.config');
const message = require('../../lib/message');

module.exports = {
    name: 'outlets',
    mixins: [DbService],

    adapter : new SqlAdapter(process.mysql.database, process.mysql.user, process.mysql.password, {
        host: process.mysql.host,
        dialect: 'mysql' /* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */,
        
    }),
    
    model: {
        name: "outlet",
        define: {
            merchantId: Sequelize.INTEGER,
            outletName: Sequelize.INTEGER,
            outletEmail: Sequelize.INTEGER,
			outletExtension: Sequelize.INTEGER,
			outletMobile: Sequelize.INTEGER,
			openingHours: Sequelize.JSON,
			status: Sequelize.STRING,
			createdBy: Sequelize.INTEGER,
			modifiedBy: Sequelize.INTEGER,
            createdAt : Sequelize.DATE,
            updatedAt : Sequelize.DATE
        },
        options: {}
    },

    actions: {

        create: {
            rest: {
				method: "POST",
				path: "/create"
            },
            async handler(ctx) {
                try{
                    const Auth = ctx.meta.user;
                    // return Auth.user
                    const adminId = Auth.role;
                    const merchantId = ctx.params.merchantId;
                    const outletName = ctx.params.outletName;
                    const outletEmail = ctx.params.outletEmail;
                    const outletExtension = ctx.params.outletExtension;
                    const outletMobile = ctx.params.outletMobile;
                    const openingHours = JSON.stringify(ctx.params.openingHours);
                    const status = 1;//active
                    const createdBy = adminId;
                    const modifiedBy = adminId;
                    if(Auth == 'forbidden'){
                        return message.message.UNAUTHORIZED;
                    }else{
                        // return adminId
                        if(adminId == 1){
                            const checkOutlet = `select * from outlets where outletName = '${outletName}'`
                            const [checkOutletress] = await this.adapter.db.query(checkOutlet);
                            if(checkOutletress != ""){
                                return message.message.UNIQOUTLET;
                            }else{
                                const outletCreate = `insert into outlets(merchantId,outletName,outletEmail,outletExtension,outletMobile,openingHours,status,createdBy,modifiedBy) values('${merchantId}','${outletName}','${outletEmail}','${outletExtension}','${outletMobile}','${openingHours}','${status}','${createdBy}','${modifiedBy}')`;
                                const [outletCreateress] = await this.adapter.db.query(outletCreate);
                                if(outletCreateress){
                                    const successMessage = {
                                        status:true,
                                        statusCode:200,
                                        message:'Save'
                                    }
                                    return successMessage
                                }else{
                                    const successMessage = {
                                        status:false,
                                        statusCode:409,
                                        message:'Note save'
                                    }
                                    return successMessage
                                }
                            }
                         
                        }else{
                            return message.message.PERMISSIONDENIDE;
                        }
                    }
                }catch(error){
                    const errMessage = {
                        success:false,
                        statusCode:409,
                        error:error.errors,
                    }
                    return errMessage;

                }
			}
        },

    },
    
}
